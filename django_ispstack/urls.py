from django.conf import settings
from django.urls import re_path
from django.contrib import admin
from django.urls import include, path

from django_ispstack_api.django_ispstack_api import urls as api_urls

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("api/", include(api_urls)),
    path(
        "qscvcapi/",
        include("django_ispstack_api_plusnet_vc.django_ispstack_api_plusnet_vc.urls"),
    ),
    path(
        "plusnetvcapi/",
        include("django_ispstack_api_plusnet_vc.django_ispstack_api_plusnet_vc.urls"),
    ),
    path(
        "telekom-de/maintenance/",
        include(
            "django_ispstack_api_telekom_de_maintenance.django_ispstack_api_telekom_de_maintenance.urls"
        ),
    ),
    path(
        "telekom-de/order/",
        include(
            "django_ispstack_api_telekom_de_order.django_ispstack_api_telekom_de_order.urls"
        ),
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
